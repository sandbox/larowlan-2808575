<?php

namespace Drupal\Tests\farmers_market\Functional;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests farmers market profile functionality.
 *
 * @group farmers_market
 */
class FarmersMarketFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'farmers_market';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
  }

  /**
   * Smoke test - tests installation completes.
   */
  public function testInstallation() {
    $this->assertTrue('Profile was successfully installed');
  }

}
